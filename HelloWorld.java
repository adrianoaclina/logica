/**
* Programa que imprime um texto na tela.
* @Author Adriano Aclina
*/
public class HelloWorld {
    // método principal
	public static void main(String[] args) {
		/*
		* escreve Hello World !
		* na tela.
		*/
		System.out.println("\"Hello World !\"");
	} // fim do void main 	
} // fim do public class 