public class Quadrado {
	public static void main(String[] args) {
		
		// desenhar um quadrado na tela
		int tamanho = 25;

		// pula linhas
		for (int i = 0; i < tamanho; i++) {
			// desenha os *
			for (int j = 0; j < tamanho; j++) {
				System.out.print("* ");
			}
			System.out.println();
			
		}
	}
}