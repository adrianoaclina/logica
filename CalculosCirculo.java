/**
* Programa que realizar diversos calculos
* com circulos
* @Author Adriano Aclina
*/
import java.util.Scanner;
public class CalculosCirculo {

	public static void main(String[] args) {
	
		Scanner teclado = new Scanner(System.in);
		System.out.println("Informe o valor do raio: ");
		double raio = teclado.nextDouble();
		// calcula o diâmetro de um círculo
		// diâmetro = 2 * raio
		//double raio = 10; // 10cm
		
		double diametro = 2 * raio;
		
		System.out.println("Diâmetro: " + diametro + " cm");
		
		// calcula a circunferência
		// circunferência = 2PIraio
		double pi = Math.PI;
		double circunferencia = 2 * pi * raio;
		System.out.println("Circunferencia: " + circunferencia + " cm");
	
		// calcula a área dos circulos
		// area  = PI * (raio * raio)
		double area = Math.PI * Math.pow(raio, 2);
		System.out.println("Area: " + area + " cm");
	}	
}