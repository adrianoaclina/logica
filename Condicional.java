public class Condicional {

	public static void main(String[] args) {
		
		int idade = 19;

		if (idade < 11) {
			System.out.println("Criança");
		}	else {
			System.out.println("Estude mais...");
		}
			

		boolean passou = true;
		if (passou) {
			System.out.println("Contratado");
		}

		int numero = 10;
		
		// int resto = 10 % 2;

		if ((numero % 2) == 0) {
			System.out.println("Par");
		} else {
			System.out.println("Impar");
		}
			
		
	}
}