public class ArrayMulti {
	public static void main(String[] args) {
		
		// vetor - array de uma dimensão
		String[] vetor = {"Ricardo", "Sandra", "Beatriz"};

		System.out.println(vetor[0]);
		System.out.println(vetor.length);

		// declara uma matriz de String
		// matriz - array de duas dimensões
		String[][] usuarios = {
			// linha 0
			{"Ricardo", "M", "DF", "Cliente"},
			{"Sandra", "F", "MG"},
			{"Beatriz", "F", "DF"}

		};

		String[][] x = new String[3][3];

		// matriz[linha] [coluna]
		System.out.print(usuarios[0][0] + ", "); // ricardo
		System.out.print(usuarios[0][1] + ", "); // M
		System.out.println(usuarios[0][2]); // MG

		// Beatriz, F, DF
		System.out.print(usuarios[2][0] + ", ");
		System.out.print(usuarios[2][1] + ", ");
		System.out.println(usuarios[2][2]);

		// quantidade de elementos 
		System.out.println(usuarios.length);
		System.out.println(usuarios[0].length);
		System.out.println(usuarios[1].length);
	}

}